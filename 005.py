n = int(input())
for _ in range(n): 
    p = list(map(int, input().split()))
    if (p[1]-p[0] == p[2]-p[1]): 
        ans = p[3] + (p[3]-p[2]) 
    else:
        ans = p[3] * (p[3]//p[2]) 
    print(p[0], p[1], p[2], p[3], ans) 

x = int(input())
for i in range(x):
    y = list(map(int,input().split()))
    a = int(y[1])-int(y[0])
    b = int(y[3])-int(y[2])
    c = int(y[1])/int(y[0])
    e = int(y[2])/int(y[1])
    if int(a) == int(b):
        d = int(b)+int(y[3])
        print(*y,str(int(d)))
    elif int(c) == int(e):
        f = int(e)*int(y[3])
        print(*y,str(int(f)))